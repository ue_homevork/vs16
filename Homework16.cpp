﻿
#include <iostream>
#include <time.h>

int main()
{
	
	const int size = 7;
	int array[size][size];
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j] << '\t';
		}
		std::cout << std::endl;
	}

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	std::cout << "current day: " << buf.tm_mday << std::endl;

	int numderStr = buf.tm_mday % size;
	std::cout << "srting index: " << numderStr << std::endl;

	int sum = 0;
	for (int i = 0; i < size ; i++)
	{ 
		sum += array[i][numderStr];
	}
	std::cout << "sum: " << sum << std::endl;
	return 0;
}

